Built following this tutorial: https://channels.readthedocs.io/en/stable/tutorial/index.html



### Developer install

First retrieve a copy of the source code for django_channels_chat:

```bash
git clone git@gitlab.com:tangibleai/django-channels-chat.git
cd django_channels_chat
```

Then, install and use the `conda` python package manager within the [Anaconda](https://www.anaconda.com/products/individual#Downloads) software package.

```bash
conda update -y -n base -c defaults conda
conda create -y -n chatenv 'python>=3.7.5,<3.9'
conda activate chatenv
pip install -r requirements.txt
```

## Run locally

```bash
docker run -p 6379:6379 -d redis:5
python manage.py runserver
```
